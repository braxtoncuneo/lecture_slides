\beamer@sectionintoc {1}{Course Learning Outcomes}{3}{0}{1}
\beamer@sectionintoc {2}{Preface}{6}{0}{2}
\beamer@sectionintoc {3}{Data Types}{14}{0}{3}
\beamer@subsectionintoc {3}{1}{Primitives}{15}{0}{3}
\beamer@subsectionintoc {3}{2}{Arrays}{17}{0}{3}
\beamer@subsectionintoc {3}{3}{Structs}{19}{0}{3}
\beamer@subsectionintoc {3}{4}{Accessing Arrays and Structs}{24}{0}{3}
\beamer@sectionintoc {4}{Storage Types}{25}{0}{4}
\beamer@subsectionintoc {4}{1}{Lifetimes}{26}{0}{4}
\beamer@subsectionintoc {4}{2}{Static Storage}{31}{0}{4}
\beamer@subsectionintoc {4}{3}{Stack Storage}{32}{0}{4}
\beamer@subsectionintoc {4}{4}{Dynamic Storage}{45}{0}{4}
\beamer@sectionintoc {5}{Pointers and Addresses}{47}{0}{5}
\beamer@subsectionintoc {5}{1}{Getting Addresses with}{48}{0}{5}
\beamer@subsectionintoc {5}{2}{Identifying Storage with Pointers}{50}{0}{5}
\beamer@subsectionintoc {5}{3}{Pointer Type Signatures}{51}{0}{5}
\beamer@subsectionintoc {5}{4}{Null Pointers}{55}{0}{5}
\beamer@subsectionintoc {5}{5}{Getting Values from Pointers}{56}{0}{5}
\beamer@subsectionintoc {5}{6}{Pointer Arithmetic}{58}{0}{5}
\beamer@subsectionintoc {5}{7}{Indexing Pointers}{60}{0}{5}
\beamer@subsectionintoc {5}{8}{Pointers vs Arrays}{61}{0}{5}
\beamer@sectionintoc {6}{Using Dynamic Memory}{62}{0}{6}
\beamer@subsectionintoc {6}{1}{Allocating and Deallocating Memory}{63}{0}{6}
\beamer@subsectionintoc {6}{2}{Common Memory Bugs}{65}{0}{6}
\beamer@subsectionintoc {6}{3}{Common Memory Use Patterns}{73}{0}{6}
