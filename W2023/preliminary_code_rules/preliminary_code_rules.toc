\beamer@sectionintoc {1}{Rules Summary}{2}{0}{1}
\beamer@sectionintoc {2}{Language Restrictions}{3}{0}{2}
\beamer@subsectionintoc {2}{1}{nullptr}{3}{0}{2}
\beamer@subsectionintoc {2}{2}{Global Variables}{4}{0}{2}
\beamer@subsectionintoc {2}{3}{auto}{5}{0}{2}
\beamer@subsectionintoc {2}{4}{goto}{6}{0}{2}
\beamer@sectionintoc {3}{Library Restrictions}{7}{0}{3}
\beamer@sectionintoc {4}{Placement}{8}{0}{4}
\beamer@subsectionintoc {4}{1}{One Condition/Statement Per Line}{8}{0}{4}
\beamer@subsectionintoc {4}{2}{Prototype Grouping}{9}{0}{4}
\beamer@subsectionintoc {4}{3}{Grouping Related Code}{10}{0}{4}
\beamer@sectionintoc {5}{Documentation Before Each Function}{11}{0}{5}
\beamer@sectionintoc {6}{Formatting}{12}{0}{6}
\beamer@subsectionintoc {6}{1}{120 character-per-line limit}{12}{0}{6}
