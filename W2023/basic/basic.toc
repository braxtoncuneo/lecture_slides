\beamer@sectionintoc {1}{Survey Results}{4}{0}{1}
\beamer@subsectionintoc {1}{1}{Some Stats}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Concerns about Learning C++}{6}{0}{1}
\beamer@subsectionintoc {1}{3}{Desired Course Material}{8}{0}{1}
\beamer@sectionintoc {2}{Course Plan}{10}{0}{2}
\beamer@sectionintoc {3}{Getting Oriented with C++}{16}{0}{3}
\beamer@subsectionintoc {3}{1}{Static vs Dynamic Typing}{16}{0}{3}
\beamer@subsectionintoc {3}{2}{Procedural vs Object Oriented}{32}{0}{3}
\beamer@subsectionintoc {3}{3}{Type Coercion}{36}{0}{3}
\beamer@subsectionintoc {3}{4}{Arguments Copy by Default}{40}{0}{3}
\beamer@subsectionintoc {3}{5}{References}{42}{0}{3}
\beamer@sectionintoc {4}{Common C++/Java/Python Features}{48}{0}{4}
\beamer@subsectionintoc {4}{1}{Arithmetic Operators}{48}{0}{4}
\beamer@subsectionintoc {4}{2}{Assignment Operators}{50}{0}{4}
\beamer@subsectionintoc {4}{3}{Scopes}{52}{0}{4}
\beamer@sectionintoc {5}{Checking our C++ Literacy}{56}{0}{5}
\beamer@subsectionintoc {5}{1}{Equivalent Expressions}{56}{0}{5}
