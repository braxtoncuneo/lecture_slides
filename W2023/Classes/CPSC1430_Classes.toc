\beamer@sectionintoc {1}{Course Learning Outcomes}{4}{0}{1}
\beamer@sectionintoc {2}{Classes}{6}{0}{2}
\beamer@subsectionintoc {2}{1}{Methods vs other Functions}{8}{0}{2}
\beamer@subsectionintoc {2}{2}{Defining Methods}{9}{0}{2}
\beamer@subsectionintoc {2}{3}{Calling Methods}{10}{0}{2}
\beamer@subsectionintoc {2}{4}{Access Specifiers}{11}{0}{2}
\beamer@subsectionintoc {2}{5}{Classes are Types}{12}{0}{2}
\beamer@subsectionintoc {2}{6}{Methods are Functions}{17}{0}{2}
\beamer@sectionintoc {3}{Why Object-Oriented Programming?}{20}{0}{3}
\beamer@subsectionintoc {3}{1}{Encapsulation}{21}{0}{3}
\beamer@subsectionintoc {3}{2}{Abstraction}{22}{0}{3}
\beamer@subsectionintoc {3}{3}{Data Hiding}{23}{0}{3}
\beamer@subsectionintoc {3}{4}{Client Accessibility}{24}{0}{3}
\beamer@sectionintoc {4}{Constructors}{25}{0}{4}
\beamer@subsectionintoc {4}{1}{What are Constructors?}{26}{0}{4}
\beamer@subsectionintoc {4}{2}{How to Declare a Constructor}{27}{0}{4}
\beamer@subsectionintoc {4}{3}{Why have Constructors?}{28}{0}{4}
\beamer@subsectionintoc {4}{4}{Overloading Constructors}{29}{0}{4}
\beamer@subsectionintoc {4}{5}{Default Constructors}{30}{0}{4}
\beamer@subsectionintoc {4}{6}{Copy Constructors}{31}{0}{4}
\beamer@subsectionintoc {4}{7}{Implicitly-declared Default Constructors}{32}{0}{4}
\beamer@sectionintoc {5}{Destructors}{33}{0}{5}
\beamer@subsectionintoc {5}{1}{What are Destructors?}{34}{0}{5}
\beamer@subsectionintoc {5}{2}{How to Declare Destructors}{35}{0}{5}
\beamer@subsectionintoc {5}{3}{Why have Destructors?}{36}{0}{5}
\beamer@sectionintoc {6}{Invariants}{37}{0}{6}
\beamer@subsectionintoc {6}{1}{Invariants}{38}{0}{6}
\beamer@subsectionintoc {6}{2}{CADRe / RAII}{39}{0}{6}
\beamer@subsectionintoc {6}{3}{Resource Ownership}{40}{0}{6}
\beamer@subsectionintoc {6}{4}{Copy Constructors and Ownership}{41}{0}{6}
\beamer@sectionintoc {7}{Overloading Operators}{42}{0}{7}
\beamer@subsectionintoc {7}{1}{What are Operators?}{43}{0}{7}
\beamer@subsectionintoc {7}{2}{The (partial) List of C++ Operators}{44}{0}{7}
\beamer@subsectionintoc {7}{3}{Overloading Operators}{45}{0}{7}
\beamer@subsectionintoc {7}{4}{Overloading the Assignment Operator}{46}{0}{7}
\beamer@sectionintoc {8}{More on Classes}{47}{0}{8}
\beamer@subsectionintoc {8}{1}{The Implicit Parameter - \tcbox [ on line, boxsep=1pt, left=0pt, right=0pt, top=0pt, bottom=0pt, colframe=grey1, colback=white, highlight math style={enhanced} ]{$\texttt {this}$}}{48}{0}{8}
\beamer@subsectionintoc {8}{2}{Making the implicit parameter \tcbox [ on line, boxsep=1pt, left=0pt, right=0pt, top=0pt, bottom=0pt, colframe=grey1, colback=white, highlight math style={enhanced} ]{$\texttt {const}$}}{49}{0}{8}
\beamer@subsectionintoc {8}{3}{ \tcbox [ on line, boxsep=1pt, left=0pt, right=0pt, top=0pt, bottom=0pt, colframe=grey1, colback=white, highlight math style={enhanced} ]{$\texttt {static}$} Class Members}{50}{0}{8}
\beamer@subsectionintoc {8}{4}{ \tcbox [ on line, boxsep=1pt, left=0pt, right=0pt, top=0pt, bottom=0pt, colframe=grey1, colback=white, highlight math style={enhanced} ]{$\texttt {static}$} Methods}{51}{0}{8}
\beamer@sectionintoc {9}{Abstract Data Types (ADTs)}{52}{0}{9}
\beamer@subsectionintoc {9}{1}{Modularization}{53}{0}{9}
\beamer@subsectionintoc {9}{2}{Interfaces}{54}{0}{9}
\beamer@subsectionintoc {9}{3}{Abstract Data Types}{57}{0}{9}
\beamer@sectionintoc {10}{Common ADTs}{58}{0}{10}
\beamer@subsectionintoc {10}{1}{Stacks}{59}{0}{10}
\beamer@subsectionintoc {10}{2}{Queues}{63}{0}{10}
\beamer@sectionintoc {11}{Exceptions}{68}{0}{11}
\beamer@subsectionintoc {11}{1}{Deques}{76}{0}{11}
\beamer@subsectionintoc {11}{2}{Lists}{77}{0}{11}
\beamer@subsectionintoc {11}{3}{Maps}{78}{0}{11}
