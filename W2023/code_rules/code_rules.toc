\beamer@sectionintoc {1}{Preface}{3}{0}{1}
\beamer@sectionintoc {2}{Summary - Rules without Automation}{4}{0}{2}
\beamer@sectionintoc {3}{Summary - Rules with Automation}{5}{0}{3}
\beamer@sectionintoc {4}{Language Restrictions}{6}{0}{4}
\beamer@subsectionintoc {4}{1}{nullptr}{6}{0}{4}
\beamer@subsectionintoc {4}{2}{Global Variables}{7}{0}{4}
\beamer@subsectionintoc {4}{3}{auto}{8}{0}{4}
\beamer@subsectionintoc {4}{4}{goto}{9}{0}{4}
\beamer@sectionintoc {5}{Library Restrictions}{10}{0}{5}
\beamer@sectionintoc {6}{Documentation Before Each Function}{11}{0}{6}
\beamer@sectionintoc {7}{General Documentation}{12}{0}{7}
\beamer@subsectionintoc {7}{1}{Comments at the Top of Files}{12}{0}{7}
\beamer@subsectionintoc {7}{2}{Naming Variables, Functions and Types}{13}{0}{7}
\beamer@subsectionintoc {7}{3}{Commenting Regions of Related Code}{16}{0}{7}
\beamer@sectionintoc {8}{Formatting}{17}{0}{8}
\beamer@subsectionintoc {8}{1}{120 character-per-line limit}{17}{0}{8}
\beamer@subsectionintoc {8}{2}{Brace Placement for Non-Control Structures}{19}{0}{8}
\beamer@subsectionintoc {8}{3}{Brace Placement for Control Structures}{20}{0}{8}
\beamer@subsectionintoc {8}{4}{Indentation}{21}{0}{8}
\beamer@subsectionintoc {8}{5}{Identifiers}{22}{0}{8}
\beamer@sectionintoc {9}{Placement}{23}{0}{9}
\beamer@subsectionintoc {9}{1}{Statements and Conditions}{23}{0}{9}
\beamer@subsectionintoc {9}{2}{Prototype Grouping}{24}{0}{9}
\beamer@subsectionintoc {9}{3}{Grouping Related Code}{25}{0}{9}
\beamer@sectionintoc {10}{Code Structure}{26}{0}{10}
\beamer@subsectionintoc {10}{1}{Functional Decomposition}{26}{0}{10}
\beamer@subsectionintoc {10}{2}{Code Clarity}{27}{0}{10}
