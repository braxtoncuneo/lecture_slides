\frametitle{\quicktag {H2}{Code Clarity}}
\begin{paratag}
\footnotesize
\textbf{Make sure code is easy to understand.}
\vspace{0.2cm}

There are many ways to solve a software problem. There are also many ways to implement the exact same function in C++. Some are much easier to understand than others.
\vspace{0.2cm}

Avoid using too many ``clever tricks''. While they are fun, they can significantly impact how easy it is to understand your code.
\end{paratag}
\begin{minipage}{0.48\textwidth}
\begin{codeblock}{Discouraged}{{}}
void make_uppercase(std::string &the_string) {
    // ?????
    char *c_string = (char *)the_string.c_str();
    // ?????
    while (*c_string) {
        // ??????
        *c_string = (*c_string < 97) ??!??!
                    (*c_string > 122) ?
                     *c_string :
                     *c_string & ~' ';
        *c_string++;
    }
}
\end{codeblock}
\end{minipage}
\hfill
\begin{minipage}{0.48\textwidth}
\begin{codeblock}{Encouraged}{{}}
void make_uppercase(std::string &the_string) {
    int size = the_string.size();
    // Loop through characters
    for (int i = 0; i < size; i++) {
        // Add the differece between 'A' and 'a'
        // if the character is between lowercase
        // 'a' and lowercase 'z'
        if ( (the_string[i]>='a') &&
             (the_string[i]<='z')     ) {
            the_string[i] += 'A' - 'a';
        }
    }
}
\end{codeblock}
\end{minipage}
