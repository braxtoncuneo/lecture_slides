\RequirePackage{pdfmanagement-testphase}
\DocumentMetadata{pdfversion=2.0,lang=en-UK,testphase=phase-II}
%\DocumentMetadata{lang=en-US,testphase=phase-II}
%\DocumentMetadata{}
% Blank slide
%\documentclass[aspectratio=169,xcolor=dvipsnames]{beamer}
\documentclass[aspectratio=169,xcolor=dvipsnames,notes]{beamer}
%\documentclass{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PACKAGES                                                  %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{lipsum}
\usepackage{../electricslide}
\usepackage{hyperref}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BEAMER SETUP                                              %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\definecolor{red}{RGB}{255,255,255}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TITLE SETUP                                               %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 

\title{\Huge \quicktag{H1}{What you Need to Know about Arrays and Pointers for Project 1}}
\date{}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% UTILITIES                                                 %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\codebox}[1] { \tcbox[
	on line, 
	boxsep=1pt,
	left=0pt,
	right=0pt,
	top=0pt,
	bottom=0pt,
	colframe=grey1,
	colback=white,  
    highlight math style={enhanced}
]{$\texttt{#1}$}}

\newcommand{\codetx}[1]{\colorbox{white}{\texttt{#1}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% START OF DOCUMENT                                         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\ExplSyntaxOn
%\makeatletter
%\cs_new:Npn \@makefntext at processX #1#2{\@makefntext at cfgpoint{#1{#2}}}
%\makeatother
%\ExplSyntaxOff

\usepackage{shortvrb}
\MakeShortVerb|

\begin{document}


% Title slide frame
\tagpdfparaOff

\begin{frame}[plain]
\titlepage
\begin{paratag}
\centering
\end{paratag}
\end{frame}

\begin{frame}[fragile]{\quicktag{H2}{Overview}}
\begin{minipage}{0.48\textwidth}
\small
\begin{itemtag}
\itag{We're doubling back to arrays to make sure you are comfortable working with them.}
\itag{These slides will also be a partial introduction to pointers.}
\itag{These concepts are meant to prepare you for P1, so please let me know if I need to clarify any of this.}
\itag{A full explanation of memory and pointers will come in later lectures}
\end{itemtag}
\end{minipage}
\hfill
\begin{minipage}{0.48\textwidth}
\centering
\figtag{\includegraphics[width=0.5\textwidth]{average_familiarity}}
\vspace{0.5cm}

\tiny From \href{https://xkcd.com/2501/}{https://xkcd.com/2501/} \\ under CC License
\end{minipage}
\end{frame}


\begin{frame}{\quicktag{H2}{Overview}}
\begin{paratag}
\tableofcontents
\end{paratag}
\end{frame}




\section{Arrays}
\subsection{What is an Array}
\begin{frame}[fragile]{\quicktag{H2}{What is an Array}}
\begin{paratag}
Arrays are \textbf{fixed-size}, \textbf{strictly ordered} collections of \textbf{homogeneously typed} values.
\end{paratag}
\begin{enumtag}
\itag{An array contains zero or more values, called \textbf{elements}.}
\itag{Every element in an array has the same type.}
\itag{Every element in an array has a unique "next" value, aside from the last element.}
\itag{Every element in an array has a unique "previous" value, aside from the first element.}
\itag{During its existance, the number of elements in an array never changes.}
\end{enumtag}
\end{frame}




\subsection{Declaring an Array}
\begin{frame}[fragile]{\quicktag{H2}{Declaring an Array}}
\fontsize{8}{2}
\begin{enumtag}
\itag{An array declaration starts with the type of its elements, followed by the name of the declared variable, followed by square brackets.}
\itag{We can define the contents of an array during declaration by providing a list of its contents in curly braces.}
\end{enumtag}
\begin{codeblock}{Array Declaration}{{}}
int main() {

  // A 3-element array of integers
  int integer_array [3];
  
  // A 4-element array of integers
  int another_array [4] = {  9, 8, 7, 6 };
  
  // A 2-element array of floats
  float a_float_array [2] = { 0.5, 1.1 };
  
  // A 3-element array of constant integers
  const long int [3] = { 1, 2, 3 };
  
  // A zero-element array of characters
  const char [0] = {};
  
    
}
\end{codeblock}
\end{frame}

\subsection{Indexing an Array}
\begin{frame}[fragile]{\quicktag{H2}{Indexing an Array}}
\begin{enumtag}
\itag{Because arrays are \textbf{strictly ordered}, they can be assigned a unique integer identifying their place in the ordering.}
\itag{The integer used to identify an element is called its index.}
\itag{In C/C++, indexes start at zero and count up.}
\end{enumtag}
\begin{codeblock}{Array Indexing}{{}}

int main() {

  // Index:              0    1    2    3
  //                     V    V    V    V
  int an_array [4] = {   9,   8,   7,   6 };
  
    
}
\end{codeblock}
\end{frame}

\begin{frame}[fragile]{\quicktag{H2}{Indexing an Array}}
\begin{enumtag}
\itag{By following the name of an array with the index of one of its elements in square brackets, we can access that element. This action is called "indexing".}
\itag{Indexing an array element behaves identically to using a variable. It can be used in an expression (reading the value) and it can be assigned to (writing the value).}
\end{enumtag}
\begin{codeblock}{Array Indexing}{{}}
#include <iostream>
int main() {
  int an_array [4] = {  9, 8, 7, 6 };
  
  std::cout << an_array[0] << std::endl; // -> 9
  std::cout << an_array[2] << std::endl; // -> 7
  
  an_array[0] = 3;
  an_array[2] = 4;
  
  std::cout << an_array[0] << std::endl; // -> 3
  std::cout << an_array[2] << std::endl; // -> 4
  return 0;
}
\end{codeblock}
\end{frame}



\subsection{Array as an Function Parameters}
\begin{frame}[fragile]{\quicktag{H2}{Arrays as Function Parameters}}
\fontsize{8}{2}
\begin{codeblock}{Arrays as Function Parameters}{{}}
// We can take an array of exact size by refernce
void pass_array_by_reference(int (&array_name)[4]) {
  array_name[0] = 1234; // We can use side effects
}

// We can also take an array of unknown size
// We should also pass in the size of the array, to be safe.
void pass_sizeless_array    (int array_name[], int array_size) {
  if ( array_size < 1) {
    return;
  }
  array_name[0] = 1234; // We can also use side effects here
}

// For reasons I have not yet explained, the below
// function is equivalent to the middle function
void pass_by_pointer        (int *array_name, int array_size) {
  if ( array_size < 1) {
    return;
  }
  array_name[0] = 1234; // We can also use side effects here
}
\end{codeblock}
\end{frame}





\subsection{Two-Dimensional Arrays}
\begin{frame}[fragile]{\quicktag{H2}{Two-Dimensional Arrays}}
\fontsize{8}{2}
\begin{codeblock}{Two-Dimensional Arrays}{{}}
#include <iostream>

int main() {

  // Here's a 2-D array
  // array of 2 arrays, each with 4 integer elements
  int a[2][4] = {
                    {8, 7, 6, 5},  // The first row
                    {4, 3, 2, 1}   // The second row
                }; 
              
  // For each additional dimension, we provide another
  // index to select along that dimension 
  std::cout << a[0][0] << std::endl; // -> 8
  std::cout << a[0][1] << std::endl; // -> 7
  std::cout << a[0][2] << std::endl; // -> 6
  std::cout << a[0][3] << std::endl; // -> 5
  
  std::cout << a[1][0] << std::endl; // -> 4
  std::cout << a[1][1] << std::endl; // -> 3
  std::cout << a[1][2] << std::endl; // -> 2
  std::cout << a[1][3] << std::endl; // -> 1
               
}
\end{codeblock}
\end{frame}


\subsection{Two-Dimensional Array Arguments}
\begin{frame}[fragile]{\quicktag{H2}{Two-Dimensional Arrays}}
\fontsize{8}{2}
\begin{codeblock}{Two-Dimensional Arrays}{{}}
// To pass 2-D arrays by reference, we add the additional
// dimension to the end of the declaration
void pass_array_by_reference(int (&array_name)[3][4]) {
  array_name[0] = 1234; // We can use side effects
}

// Add another set of empty braces for the second dimension
void pass_by_reference      (int array_name[][], int height, int width) {
  if ( (height < 1) || (width < 1) ) {
    return;
  }
  array_name[0][0] = 1234; // We can also use side effects here
}
\end{codeblock}
\end{frame}


\section{Allocating a 2-Dimensional Array Dynamically}
\begin{frame}[fragile]{\quicktag{H2}{Allocating a 2-Dimensional Array Dynamically}}
\fontsize{8}{2}
\begin{codeblock}{Allocating a 2-Dimensional Array Dynamically}{{}}
// Allocates a 2-D array of the given height and width
// The returned variable should behave just like a normal
// 2D array. The only difference is that it has the type
// signature "int**".
int** alloc_2d_int_array(int height, int width) {

  // Allocate the array of pointers
  int ** integer_grid = new int*[height];

  // Allocate an array for each pointer
  for(int i=0; i<height; i++){
    integer_grid[i] = new int[width];
  }
  
  // returns our 2D array
  return integer_grid;
}
\end{codeblock}
\end{frame}



\subsection{Passing a Dynamic 2D Array as an Argument}
\begin{frame}[fragile]{\quicktag{H2}{Passing a Dynamic 2D Array as an Argument}}
\fontsize{8}{2}
\begin{codeblock}{Passing a Dynamic 2D Array as an Argument}{{16-21}}
// NOTE: THIS ONLY WORKS FOR DYNAMICALLY ALLOCATED 2-D Arrays

// Add another asterisk for the extra dimension
void pass_by_pointer        (int **array_name, int height, int width) {
  if ( (height < 1) || (width < 1) ) {
    return;
  }
  array_name[0][0] = 1234; // We can use side effects here
}
\end{codeblock}
\end{frame}


\end{document}