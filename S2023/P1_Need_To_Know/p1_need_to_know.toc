\beamer@sectionintoc {1}{Arrays}{4}{0}{1}
\beamer@subsectionintoc {1}{1}{What is an Array}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Declaring an Array}{5}{0}{1}
\beamer@subsectionintoc {1}{3}{Indexing an Array}{6}{0}{1}
\beamer@subsectionintoc {1}{4}{Array as an Function Parameters}{8}{0}{1}
\beamer@subsectionintoc {1}{5}{Two-Dimensional Arrays}{9}{0}{1}
\beamer@subsectionintoc {1}{6}{Two-Dimensional Array Arguments}{10}{0}{1}
\beamer@sectionintoc {2}{Allocating a 2-Dimensional Array Dynamically}{11}{0}{2}
\beamer@subsectionintoc {2}{1}{Passing a Dynamic 2D Array as an Argument}{12}{0}{2}
