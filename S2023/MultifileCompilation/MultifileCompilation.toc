\beamer@sectionintoc {1}{Course Learning Outcomes}{3}{0}{1}
\beamer@sectionintoc {2}{Why Multiple Files?}{4}{0}{2}
\beamer@sectionintoc {3}{Compiling in Parts}{6}{0}{3}
\beamer@sectionintoc {4}{Header Files (.h)}{10}{0}{4}
\beamer@sectionintoc {5}{Source Files (.cpp)}{12}{0}{5}
\beamer@sectionintoc {6}{Makefiles}{14}{0}{6}
