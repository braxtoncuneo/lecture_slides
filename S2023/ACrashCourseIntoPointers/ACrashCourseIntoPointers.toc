\beamer@sectionintoc {1}{Processor Basics}{3}{0}{1}
\beamer@sectionintoc {2}{Fundamental Questions}{4}{0}{2}
\beamer@sectionintoc {3}{The Arithmetic Logic Unit}{5}{0}{3}
\beamer@sectionintoc {4}{Registers}{6}{0}{4}
\beamer@sectionintoc {5}{Routing Input}{7}{0}{5}
\beamer@sectionintoc {6}{Routing Output}{8}{0}{6}
\beamer@sectionintoc {7}{Random Access Memory}{9}{0}{7}
\beamer@sectionintoc {8}{The Control Unit}{10}{0}{8}
\beamer@sectionintoc {9}{The Instruction Counter}{12}{0}{9}
\beamer@sectionintoc {10}{An Example of the Instruction Counter}{13}{0}{10}
\beamer@sectionintoc {11}{Where are our variables?}{14}{0}{11}
\beamer@sectionintoc {12}{A Big Point about Types}{15}{0}{12}
\beamer@sectionintoc {13}{What do the bits mean?}{16}{0}{13}
