\beamer@sectionintoc {1}{Course Learning Outcomes}{3}{0}{1}
\beamer@sectionintoc {2}{What is a ``Type''?}{4}{0}{2}
\beamer@subsectionintoc {2}{1}{Modules and Interfaces}{7}{0}{2}
\beamer@subsectionintoc {2}{2}{Abstract Data Types}{12}{0}{2}
\beamer@sectionintoc {3}{What is a ``List''?}{15}{0}{3}
\beamer@subsectionintoc {3}{1}{Array-Based Lists}{17}{0}{3}
\beamer@subsectionintoc {3}{2}{Linked Lists}{24}{0}{3}
\beamer@subsectionintoc {3}{3}{Takeaways about Data Structures}{33}{0}{3}
\beamer@sectionintoc {4}{Useful Linked List Patterns}{34}{0}{4}
