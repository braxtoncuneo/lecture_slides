\RequirePackage{pdfmanagement-testphase}
\DocumentMetadata{pdfversion=2.0,lang=en-UK,testphase=phase-II}

% Blank slide
\documentclass[aspectratio=169,xcolor=dvipsnames,notes]{beamer}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PACKAGES                                                  %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{lipsum}
\usepackage{multicol}
\usepackage{../electricslide}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TITLE SETUP                                               %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 
\title{\Huge ADTs and Lists}
\date{}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% START OF DOCUMENT                                         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{document}

% Title slide frame
\begin{frame}[plain]

\titlepage

\setbeamertemplate{footline}{Braxton Cuneo}


% Background image

%\begin{tikzpicture}[remember picture,overlay]
%    \node[above right,inner sep=0pt] at (current page.south west)
%    {
%        \includegraphics[width=\paperwidth]{Titleimage.png}
%    };
%\end{tikzpicture}

\end{frame}


\begin{frame}
\frametitle{\quicktag{H2}{Overview}}
    \begin{columns}[t]
        \begin{column}{.5\textwidth}
            \tableofcontents[sections={1-4}]
        \end{column}
        \begin{column}{.5\textwidth}
            \tableofcontents[sections={5-8}]
        \end{column}
    \end{columns}
\end{frame}


\section{Course Learning Outcomes}

\begin{frame}{\quicktag{H2}{Course Learning Outcomes}}

\begin{minipage}{0.48\textwidth}

\quicktag{H3}{{\Large \textbf{Covered Here}}}
\begin{enumtag}

\itagl{\faCheckSquare}{Linked Lists}

\itagl{\faCheckSquare}{Doubly Linked Lists}

\itagl{\faCheckSquare}{Circular Linked Lists}

\itagl{\faCheckSquare}{Trade-offs of Linked Lists vs Arrays}

\itagl{\faCheckSquare}{Abstract data types}

\end{enumtag}
\vspace{1cm}
\end{minipage}
\hfill
\begin{minipage}{0.48\textwidth}
\quicktag{H3}{{\Large \textbf{Covered Here}}}
\begin{enumtag}
\itagl{\faCheckSquare}{Inserting Elements}

\itagl{\faCheckSquare}{Removing Elements}

\itagl{\faCheckSquare}{Finding Elements}

\itagl{\faCheckSquare}{Introduce modularization, decomposition, and code structuring}

\itagl{\faCheckSquare}{Design and implement functionality for abstract data types\\ (stacks, queues,...) }

\itagl{\faCheckSquare}{Understand the distinction between client code and ADT implementation}

\vspace{1.75cm}
\end{enumtag}
\end{minipage}

\end{frame}


\section{What is a ``Type''?}

\begin{frame}[plain]
\title{\quicktag{H2}{\Huge What is a ``Type''?}}
\titlepage
\end{frame}


\begin{frame}[fragile]{\quicktag{H2}{What is a ``Type''?}}

\begin{minipage}{0.48\textwidth}

\begin{paratag}
\Large ~\\It depends upon who you ask.
\end{paratag}
\vspace{0.5cm}
\quicktag{H3}{{\Large \textbf{To a compiler:~\\}}}
\Large
\begin{enumtag}
\itag{A set of possible ``values'', expressed as a set of fields.~\\~\\}
\itag{A way to determine which function definition (if any) is valid to execute for an argument sequence.}
\end{enumtag}
\vspace{1cm}
\end{minipage}
\hfill
\begin{minipage}{0.48\textwidth}
\begin{codeblock}{}{{},fontsize=\tiny}
struct TypeA {};

struct TypeB {};

void report(TypeA input){
	std::cout << "The input is TypeA\n";
}

void report(TypeB input){
	std::cout << "The input is TypeB\n";
}
\end{codeblock}
\end{minipage}
\end{frame}



\begin{frame}[fragile]{\quicktag{H2}{What is a ``Type''?}}

\begin{paratag}
\Large It depends upon who you ask.
\end{paratag}
\vspace{0.5cm}
\quicktag{H3}{{\Large \textbf{To a person:~\\}}}
\large
\begin{enumtag}
\itag{What part of the program does it represent? \faArrowRight ~ Module ~\\~\\}
\itag{What can we do with it? \faArrowRight ~ Interface ~\\~\\}
\itag{What does it require from us? \faArrowRight ~ Method Preconditons ~\\~\\}
\itag{What does it guarantee for us? \faArrowRight ~ Method Postconditions ~\\~\\}
\itag{What should always be true about it? \faArrowRight ~ Invariants}
\end{enumtag}

\end{frame}

\subsection{Modules and Interfaces}

\begin{frame}[fragile]{\quicktag{H2}{Modularization}}
\begin{minipage}{0.38\textwidth}
\begin{itemtag}
\itag{\textbf{Modularization} is the process of dividing functionality into independent pieces (modules) which can be used interchangeably.}
\vspace{0.2cm}

\itag{When we break down a program into a set of types (with proper encapsulation), we are performing a type of \textbf{modularization}.}
\end{itemtag}
\end{minipage}
\hfill
\begin{minipage}{0.58\textwidth}
\includesvg[width=\textwidth]{Modularization.svg}
\end{minipage}
\end{frame}

\begin{frame}[fragile]{\quicktag{H2}{Interfaces}}
\begin{minipage}{0.38\textwidth}
\begin{itemtag}
\itag{An \textbf{interface} is a collection of methods/functions which are used to communicate information across a software boundary.}
\vspace{0.2cm}

\itag{The public members/ methods of a type make up the \textbf{interface} of that type/module.}
\end{itemtag}
\end{minipage}
\hfill
\begin{minipage}{0.58\textwidth}
\includesvg[width=\textwidth]{Modularization.svg}
\end{minipage}
\end{frame}

\begin{frame}[fragile]{\quicktag{H2}{Interfaces}}
\begin{minipage}{0.38\textwidth}
\begin{paratag}
\Large If two classes implement a common set of members/methods, with matching names and type/function signatures, they are said to implement the same interface.
\end{paratag}
\end{minipage}
\hfill
\begin{minipage}{0.58\textwidth}
\includesvg[width=\textwidth]{Interfaces.svg}
\end{minipage}
\end{frame}



\begin{frame}[fragile]{\quicktag{H2}{Interfaces}}
\begin{minipage}{0.38\textwidth}
\begin{itemtag}
\itag{Importantly, classes which implement the same interface do \textbf{not} necessarily behave the same way.}
\vspace{0.2cm}

\itag{Interfaces cover only the way information is communicated across boundaries, not how that communication affects execution.}
\end{itemtag}
\end{minipage}
\hfill
\begin{minipage}{0.58\textwidth}
\includesvg[width=\textwidth]{Interfaces.svg}
\end{minipage}
\end{frame}




\begin{frame}[fragile]{\quicktag{H2}{An Example of Equivalent Interfaces}}
\begin{minipage}{0.48\textwidth}
\begin{codeblock}{One Interface\\ Implementation}{{}}
class MaximumTracker {
    private:
    int record;

    public:
    MaximumTracker() {
        record = -100;
    }
    void attempt_record(int attemptValue) {
        if (attemptValue > record) {
            record = attemptValue;
        }
    }
    int get_record() {
        return record;
    }
};
\end{codeblock}
\end{minipage}
\hfill
\begin{minipage}{0.48\textwidth}
\begin{codeblock}{Another Implementation of\\ the Same Interface}{{}}
class MinimumTracker {
    private:
    int record;

    public:
    MinimumTracker() {
        record = 100;
    }
    void attempt_record(int attemptValue) {
        if (attemptValue < record) {
            record = attemptValue;
        }
    }
    int get_record() {
        return record;
    }
};
\end{codeblock}
\end{minipage}
\end{frame}



\subsection{Abstract Data Types}
\begin{frame}[fragile]{\quicktag{H2}{Abstract Data Types}}
\begin{minipage}{0.78\textwidth}
\begin{itemtag}
\itag{An \textbf{abstract data type} is a set of types which implement the same interface, and which have a common set of pre-conditions and post-conditions upon that interface.}
\vspace{0.3cm}

\itag{\textbf{Importantly}, members of an abstract data type \textbf{do not} need to behave identically or be implemented identically.}
\vspace{0.3cm}

\itag{A type implements an abstract data type so long as all interface components are implemented and the required set of pre-conditions and post-conditions are enforced.}
\end{itemtag}
\end{minipage}
\hfill
\begin{minipage}{0.18\textwidth}
\includesvg[width=\textwidth]{Candles.svg}
\end{minipage}
\end{frame}



\begin{frame}[fragile]{\quicktag{H2}{Abstract Data Types}}
\begin{minipage}{0.38\textwidth}
\begin{itemtag}
\itag{In other words, an \textbf{abstract data type} defines a set of externally-observable properties/behaviors.}
\vspace{0.2cm}

\itag{If two types share such a set of properties/behaviors, they both belong to a corresponding data type.}
\end{itemtag}
\end{minipage}
\hfill
\begin{minipage}{0.58\textwidth}
\includesvg[width=\textwidth]{ADT.svg}
\end{minipage}
\end{frame}


\begin{frame}[fragile]{\quicktag{H2}{Abstract Data Types}}
\begin{minipage}{0.38\textwidth}
\begin{paratag}
Importantly, since an \textbf{ADT} can belong to multiple \textbf{ADTs}, and two types may share some \textbf{ADTs} in common, but not others.
\end{paratag}
\end{minipage}
\hfill
\begin{minipage}{0.58\textwidth}
\includesvg[width=\textwidth]{ADT.svg}
\end{minipage}
\end{frame}



\section{What is a ``List''?}

\begin{frame}[plain]
\title{\quicktag{H2}{\Huge What is a ``List''?}}
\titlepage
\end{frame}

\begin{frame}{\quicktag{H2}{What is a ``List''?}}

\begin{minipage}{0.48\textwidth}
\quicktag{H3}{{\Large \textbf{Some General Requirements}}}
\begin{enumtag}
\itag{Contains some number of ``elements'', which contain values}

\itag{The elements have an ``ordering'' relative to one-another, with some elements ``after'' others.}
\end{enumtag}
\vspace{1.65cm}
\end{minipage}
\hfill
\begin{minipage}{0.48\textwidth}
\quicktag{H3}{{\Large \textbf{Potential Additions}}}
\begin{enumtag}
\itag{Can elements be added or removed? Where?}
\itag{Are element's positions stable in memory?}
\itag{Can all elements be looked up by the same base address?}
\itag{Where can values be added or removed?}
\end{enumtag}
\end{minipage}

\end{frame}


\subsection{Array-Based Lists}
\begin{frame}[plain]
\title{\quicktag{H2}{\Huge Array-Based Lists}}
\titlepage
\end{frame}

\begin{frame}{\quicktag{H2}{Using Arrays to Store Elements}}
\LARGE
\begin{itemtag}
\itag{Elements stored contiguously in memory}
\vspace{1cm}
\itag{Allocated with a fixed number of elements}
\vspace{1cm}
\itag{All elements can be accessed by indexing a pointer to its first element (its \textbf{base address})}
\end{itemtag}
\begin{minipage}{0.48\textwidth}
\end{minipage}
\end{frame}


\begin{frame}{\quicktag{H2}{Array-based lists which move elements}}
\begin{paratag}
\Large
Array-based lists that move elements between backing arrays can increase their capacity beyond what was allocated during construction.
\end{paratag}
\vspace{1cm}
\centering
\includesvg[inkscapelatex=false, width=\textwidth]{move_array.svg}
\end{frame}



\begin{frame}{\quicktag{H2}{Array-based lists which do not move elements}}
\begin{paratag}
\Large
Array-based lists that \textbf{do not} move elements between different arrays/indexes guarantee consistent memory addresses for all elements.
\end{paratag}
\vspace{1cm}
\centering
\includesvg[inkscapelatex=false, width=\textwidth]{no_move_array.svg}
\end{frame}


\begin{frame}{\quicktag{H2}{Array-based lists with a tracked ``size''}}
\begin{paratag}
\Large
Tracking a ``size'' separate from the backing array's true size (capacity) allows lists to: 
\end{paratag}
\begin{itemtag}
\itag{change their element count without allocating a new array}
\itag{add or remove elements at the end of the list without moving other elements}
\end{itemtag}
\centering
\includesvg[inkscapelatex=false, width=\textwidth]{size_array.svg}
\end{frame}



\begin{frame}{\quicktag{H2}{Array-based lists with a tracked ``size'' and ``front''}}
\begin{paratag}
\Large
Tracking both a ``size'' and a ``front'' allows lists to add to or remove from the start or end of their elements without moving pre-existing elements.
\end{paratag}
\vspace{0.5cm}
\centering
\includesvg[inkscapelatex=false, width=\textwidth]{size_front_array.svg}
\end{frame}



\begin{frame}{\quicktag{H2}{Limitations of Array-based Lists}}
\Large
\begin{itemtag}
\itag{Cannot insert elements in between pre-existing elements without moving them}
\vspace{0.5cm}
\itag{The bigger an array-based list is, the more elements need to be moved if an element is inserted in the middle}
\vspace{0.5cm}
\itag{Cannot increase capacity beyond the amount allotted during construction without moving elements}
\end{itemtag}
\end{frame}



\subsection{Linked Lists}
\begin{frame}[plain]
\title{\quicktag{H2}{\Huge Linked Lists}}
\titlepage
\end{frame}


\begin{frame}{\quicktag{H2}{Linked Lists}}
\begin{minipage}{0.48\textwidth}
\quicktag{H3}{{\Large \textbf{In a Linked List:}}}
\vspace{0.5cm}
\begin{itemtag}
\itag{Each element is stored next to a pointer in a ``Node'' data type}
\vspace{0.5cm}
\itag{The pointer stored with an element identifies the node which contains the next element in the list's ordering}
\vspace{0.5cm}
\itag{If a node pointer is \codebox{NULL}, there are no further elements in the ordering}
\end{itemtag}
\end{minipage}
\hfill
\begin{minipage}{0.48\textwidth}
\centering
\vspace{0.5cm}
\includesvg[inkscapelatex=false, width=\textwidth]{linked_list.svg}
\end{minipage}
\end{frame}


\begin{frame}{\quicktag{H2}{Notable Properties of Linked Lists}}
\quicktag{H3}{{\Large \textbf{In a Linked List:}}}
\vspace{0.5cm}
\begin{itemtag}
\itag{Each element is stored independently - \textbf{elements can be inserted/removed anywhere without changing the address of other elements.}}
\vspace{0.5cm}
\itag{Each element may be allocated independently - \textbf{the lifetimes of elements in the same list can be very different. They could even be held in different types of storage!}}
\vspace{0.5cm}
\itag{The ordering of elements is determined by the pointers stored alongside those elements - \textbf{the ordering of elements is not bound by the same rules as arrays. Elements may simultaneously exist in different lists!}}
\end{itemtag}
\end{frame}



\begin{frame}{\quicktag{H2}{Singly-Linked Lists}}
\begin{paratag}
\Large \textbf{A singly-linked list contains only one pointer per node.}
\end{paratag}
\vspace{1.0cm}
\centering
\includesvg[inkscapelatex=false, width=\textwidth]{singly_linked.svg}
\end{frame}



\begin{frame}{\quicktag{H2}{Ordering in Singly-Linked Lists}}
\begin{paratag}
\Large \bfseries
How to interpret the order of a singly linked list is dependent upon the use case.~\\~\\
For example, it may be useful to link in reverse of the conventional element ordering.
\end{paratag}
\vspace{0.5cm}
\centering
\includesvg[inkscapelatex=false, width=\textwidth]{rev_linked.svg}
\end{frame}



\begin{frame}{\quicktag{H2}{Doubly-Linked Lists}}
\begin{paratag}
\Large \textbf{A doubly-linked list contains two pointers per node, pointing forward and backward through the list ordering.}
\end{paratag}
\vspace{1.0cm}
\centering
\includesvg[inkscapelatex=false, width=\textwidth]{doubly_linked.svg}
\end{frame}


\begin{frame}{\quicktag{H2}{Circular Linked Lists}}
\begin{paratag}
\Large \textbf{A circular linked list has an ordering that loops.}
\end{paratag}
\vspace{1.0cm}
\centering
\includesvg[inkscapelatex=false, width=\textwidth]{circular_linked.svg}
\end{frame}




\begin{frame}{\quicktag{H2}{Overlapping Linked Lists}}
\begin{paratag}
\Large \textbf{Because the ordering defined by linked lists is so flexible, it is possible to have multiple linked lists share the same elements.}
\end{paratag}
\vspace{0.5cm}
\centering
\includesvg[inkscapelatex=false, width=\textwidth]{over_linked.svg}
\end{frame}


\begin{frame}{\quicktag{H2}{Limitations of Linked Lists}}
\Large
\begin{itemtag}
\itag{Traversing (iterating across) a linked list generally requires more reads from memory, because advancing forward to each node in the middle of a list requires retrieving its address. }
\vspace{0.5cm}
\itag{Requires a pointer to be stored for every element. This adds extra memory use for every element stored by a list.}
\vspace{0.5cm}
\itag{Modern processors are good at fetching values that are close to each other, and the elements in a linked list are usually spread far apart.}
\end{itemtag}
\end{frame}



\subsection{Takeaways about Data Structures}
\begin{frame}[fragile]{\quicktag{H2}{Takeaways about Data Structures}}
\begin{minipage}{0.38\textwidth}
\begin{paratag}
The relationship between arrays, dynamic arrays, and linked lists reveals something fundamental. \textbf{We cannot have all three of these at the same time.}
\vspace{0.2cm}
\end{paratag}
\begin{itemtag}
\itag{Adding/removing fields}
\vspace{0.2cm}

\itag{Contiguity of fields}
\vspace{0.2cm}

\itag{Address stability of fields}
\end{itemtag}
\end{minipage}
\hfill
\begin{minipage}{0.58\textwidth}
\includesvg[width=\textwidth]{PickTwo.svg}
\end{minipage}
\end{frame}



\iffalse
\subsection{Removing Limitations: Partial Ordering}
\begin{frame}[plain]
\title{\quicktag{H2}{\Huge Removing Limitations: Partial Ordering}}
\titlepage
\end{frame}

\begin{frame}[fragile]{\quicktag{H2}{Removing Limitations: Partial Ordering}}
\begin{minipage}{0.38\textwidth}
\begin{paratag}
What properties (ADTs) can one get from loosening the concept of ``ordering''?
\vspace{0.2cm}

\end{paratag}
\begin{itemtag}
\itag{Adding to the front}
\vspace{0.2cm}

\itag{Adding to the back}
\vspace{0.2cm}

\itag{Removing from the front}
\vspace{0.2cm}

\itag{Removing from the back}
\end{itemtag}
\end{minipage}
\hfill
\begin{minipage}{0.58\textwidth}
\includesvg[width=0.95\textwidth]{ListMap.svg}
\end{minipage}
\end{frame}

\begin{frame}[fragile]{\quicktag{H2}{Removing Limitations: Partial Ordering}}
\begin{minipage}{0.38\textwidth}
\begin{paratag}
\Large
What properties (ADTs) can one get from \textbf{adding restrictions}?
\vspace{0.2cm}

More on this in the next module!
\end{paratag}

\end{minipage}
\hfill
\begin{minipage}{0.58\textwidth}
\includesvg[width=0.95\textwidth]{ListMapMystery.svg}
\end{minipage}
\end{frame}



\subsection{Adding Limitations: List Subtypes}
\begin{frame}[plain]
\title{\quicktag{H2}{\Huge Adding Limitations: List Subtypes}}
\titlepage
\end{frame}


\begin{frame}[plain]
\title{\quicktag{H2}{\Huge Adding Limitations: List Subtypes}}
\titlepage
\end{frame}

\begin{frame}[fragile]{\quicktag{H2}{Adding Limitations: List Subtypes}}
\begin{minipage}{0.38\textwidth}
\begin{paratag}
What properties (ADTs) can one get from adding limitations to lists?


\vspace{0.2cm}
Assuming that elements can only be added/removed at the front/back of a list, what do we get if we allow:
\vspace{0.2cm}

\end{paratag}
\begin{itemtag}
\itag{Adding to the front}
\vspace{0.2cm}

\itag{Adding to the back}
\vspace{0.2cm}

\itag{Removing from the front}
\vspace{0.2cm}

\itag{Removing from the back}
\end{itemtag}
\end{minipage}
\hfill
\begin{minipage}{0.58\textwidth}
\includesvg[width=\textwidth]{SQVenn.svg}
\end{minipage}
\end{frame}

\begin{frame}[fragile]{\quicktag{H2}{Stacks}}
\begin{minipage}{0.48\textwidth}
\begin{paratag}
The \textbf{stack} ADT consists of two operations (methods):
\vspace{0.3cm}

\end{paratag}
\begin{itemtag}
\itag{\textbf{push} accepts a single value as input and stores that value in a collection}
\vspace{0.3cm}

\itag{\textbf{pop} removes the \textbf{most recently} added value that is still in its collection, and returns the removed value}

\itagl{\faPlusCircle}{An alternate version of the \textbf{stack} ADT returns the topmost value non-destructively through a \textbf{top} method.}
\end{itemtag}
\end{minipage}
\hfill
\begin{minipage}{0.48\textwidth}
\figtag{\includesvg[inkscapelatex=false,width=\textwidth]{PushPop}}
\end{minipage}
\end{frame}


\fi


\section{Useful Linked List Patterns}
\begin{frame}[plain]
\title{\quicktag{H2}{\Huge Useful Linked List Patterns}}
\titlepage
\end{frame}


\begin{frame}[fragile]{\quicktag{H2}{Identifying Linked Lists with Pointers}}
\begin{paratag}
\large \bfseries
In order to use a linked list, a program must have a pointer/reference identifying a node in that list. If the list is singly-linked, a pointer to the first node is required in order to have access to the whole thing~\\~\\
The pointer to this first node may be stored inside a struct for convenience, or to bundle it with additional information about the list.~\\~\\
\end{paratag}

\begin{minipage}{0.48\textwidth}
\begin{codeblock}{Raw Pointer Parameter}{{},fontsize=\tiny}
struct Node {
  int   data;
  Node *next;
};

bool is_empty(Node* head) {
	return ( head == nullptr );
}
\end{codeblock}
\end{minipage}
\hfill
\begin{minipage}{0.48\textwidth}
\begin{codeblock}{List Struct Parameter}{{},fontsize=\tiny}
struct List {
  int   size;
  Node *head;
};

bool is_empty(List linked_list) {
	return ( linked_list.head == nullptr );
}
\end{codeblock}
\end{minipage}
\end{frame}



\begin{frame}[fragile]{\quicktag{H2}{Finding Elements in Linked Lists}}
\begin{paratag}
\large \bfseries
To iterate through a linked list and inspect values, a pointer must be used to ``hop'' through the list - using the node currently identified by the pointer to get the address of the next node.
\end{paratag}

\begin{minipage}{0.48\textwidth}
\begin{codeblock}{Linked List Search}{{},fontsize=\tiny}
struct Node {
	Item  data;
	Node *next; 
};

bool search_test(Item data);

Node *find_node (Node *head) {
    Node *iterator = head;
    while( iterator != nullptr ){
        if( search_test(iterator->data) ){
            return iterator;
        }
        iterator = iterator->next;
    }
    return nullptr; // for if nothing is found
}
\end{codeblock}
\end{minipage}
\hfill
\begin{minipage}{0.48\textwidth}
\centering
\includesvg[inkscapelatex=false, width=0.8\textwidth]{finding_links.svg}
\end{minipage}
\end{frame}



\begin{frame}[fragile]{\quicktag{H2}{Inserting Elements into Linked Lists}}
\begin{paratag}
\large \bfseries
To insert one node ($X$) before another node ($Y$), the pointer identifying $Y$ in the ordering must be set to identify $X$ and the ``next'' pointer of $X$ must be set to identify $Y$.
\end{paratag}

\begin{minipage}{0.48\textwidth}
\begin{codeblock}{Insert After}{{},fontsize=\tiny}
// Logic for handling null pointers intentionally
// excluded
void insert_after (Node *target, Node *inserted ){   
    Node *temp = target->next;
    target->next = inserted;
    inserted->next = temp;
}

void insert_front (List &the_list, Node *inserted ){   
    inserted->next = the_list.head;
    the_list.head = inserted;
}
\end{codeblock}
\end{minipage}
\hfill
\begin{minipage}{0.48\textwidth}
\centering
\includesvg[inkscapelatex=false, width=0.8\textwidth]{adding_links.svg}
\end{minipage}
\end{frame}





\begin{frame}[fragile]{\quicktag{H2}{Removing Elements from Linked Lists}}
\begin{paratag}
\large \bfseries
To remove a bide ($X$) from a linked list, the pointer identifying $X$ in the ordering must be set to the ``next'' value of $X$.
\end{paratag}

\begin{minipage}{0.48\textwidth}
\begin{codeblock}{Insert After}{{},fontsize=\tiny}
// Logic for handling null pointers intentionally
// excluded
Node *remove_after (Node *target){
    Node *removed = target->next;
    target->next = target->next->next;
    return removed;
}

Node *remove_front(List &the_list){
    Node *removed = the_list.head;
    the_list.head = the_list.head->next;
    return removed;
}
\end{codeblock}
\end{minipage}
\hfill
\begin{minipage}{0.48\textwidth}
\centering
\includesvg[inkscapelatex=false, width=0.8\textwidth]{removing_links.svg}
\end{minipage}
\end{frame}






\end{document}