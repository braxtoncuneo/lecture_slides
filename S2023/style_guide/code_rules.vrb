\frametitle{\quicktag {H2}{Code Clarity}}
\begin{paratag}
\footnotesize
\textbf{Make sure code is easy to understand.}
\vspace{0.2cm}

There are many ways to solve a software problem. There are also many ways to implement the exact same function in C++. Some are much easier to understand than others.
\vspace{0.2cm}

Avoid using too many ``clever tricks''. While they are fun, they can significantly impact how easy it is to understand your code.
\end{paratag}
\begin{minipage}{0.48\textwidth}
\begin{codeblock}{Discouraged}{{}}
void makeUppercase(std::string& theString)
{
	// ?????
	char* cString = (char*) theString.c_str();
	// ?????
	while(*cString){
		// ??????
		*cString =  (*cString < 97)  ??!??!
		            (*cString > 122)  ?
		             *cString :
		             *cString  & ~ ' ';
		*cString++;
	}
}
\end{codeblock}
\end{minipage}
\hfill
\begin{minipage}{0.48\textwidth}
\begin{codeblock}{Encouraged}{{}}
void makeUppercase(std::string& theString)
{
	int size = theString.size();
	// Loop through characters
	for (int i=0; i<size; i++) {
		// Add the differece between 'A' and 'a'
		// if the character is between lowercase
		// 'a' and lowercase 'z'
		if((theString[i]>='a') && (theString<='z')) {
			theString[i] += 'A' - 'a';
		}
	}
}
\end{codeblock}
\end{minipage}
