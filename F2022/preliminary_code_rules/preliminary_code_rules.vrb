\frametitle{\quicktag {H2}{Formatting - Per-line Character Limit}}
\begin{textblock}{\small Maximum 120 characters per-line, except for long strings.}
Some of the code you may work with in the future could be read by a lot of people. Some of them need large-print text to read. All of them will want to understand it. If you have a single expression that is \textbf{so long} that it extends past 120 characters, it could be difficult for others to understand.

\vspace{0.3cm}
I make an exception for expressions that use long string literals, because breaking up string literals across multiple lines can be less readable than having long single-line literals.
\end{textblock}
\begin{codeblock}{Allowed}{{}}
int my_value = some_function((12345 + another_value) / 1072)
               - another_function(x,y,z,q)
               % a_third_function("a short string literal");
std::cout << function_a("The rabbit-hole went straight on like a tunnel for some way, and then dipped suddenly down, so suddenly that Alice had not a moment to think about stopping herself before she found herself falling down...") + function_b();
\end{codeblock}
