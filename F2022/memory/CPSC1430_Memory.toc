\beamer@sectionintoc {1}{Course Learning Outcomes}{3}{0}{1}
\beamer@sectionintoc {2}{Preface}{5}{0}{2}
\beamer@sectionintoc {3}{Types in Memory}{14}{0}{3}
\beamer@subsectionintoc {3}{1}{Primitives}{14}{0}{3}
\beamer@subsectionintoc {3}{2}{Arrays}{16}{0}{3}
\beamer@subsectionintoc {3}{3}{Structs}{18}{0}{3}
\beamer@subsectionintoc {3}{4}{Accessing Arrays and Structs}{23}{0}{3}
\beamer@sectionintoc {4}{Types of Storage}{24}{0}{4}
\beamer@subsectionintoc {4}{1}{Lifetimes}{24}{0}{4}
\beamer@subsectionintoc {4}{2}{Static Storage}{30}{0}{4}
\beamer@subsectionintoc {4}{3}{Stack Storage}{31}{0}{4}
\beamer@subsectionintoc {4}{4}{Dynamic Storage}{44}{0}{4}
\beamer@sectionintoc {5}{Pointers and Addresses}{46}{0}{5}
\beamer@subsectionintoc {5}{1}{Getting Addresses with}{46}{0}{5}
\beamer@subsectionintoc {5}{2}{Identifying Storage with Pointers}{48}{0}{5}
\beamer@subsectionintoc {5}{3}{Pointer Type Signatures}{49}{0}{5}
\beamer@subsectionintoc {5}{4}{Null Pointers}{53}{0}{5}
\beamer@subsectionintoc {5}{5}{Getting Values from Pointers}{54}{0}{5}
\beamer@subsectionintoc {5}{6}{Pointer Arithmetic}{56}{0}{5}
\beamer@subsectionintoc {5}{7}{Indexing Pointers}{58}{0}{5}
\beamer@subsectionintoc {5}{8}{Pointers vs Arrays}{59}{0}{5}
\beamer@sectionintoc {6}{Using Dynamic Memory}{60}{0}{6}
\beamer@subsectionintoc {6}{1}{Allocating and Deallocating Memory}{60}{0}{6}
\beamer@subsectionintoc {6}{2}{Common Memory Bugs}{62}{0}{6}
\beamer@subsectionintoc {6}{3}{Common Memory Use Patterns}{70}{0}{6}
