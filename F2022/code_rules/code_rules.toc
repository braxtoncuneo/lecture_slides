\beamer@sectionintoc {1}{Preface}{3}{0}{1}
\beamer@sectionintoc {2}{Rules Summary}{4}{0}{2}
\beamer@sectionintoc {3}{Language Restrictions}{5}{0}{3}
\beamer@subsectionintoc {3}{1}{nullptr}{5}{0}{3}
\beamer@subsectionintoc {3}{2}{Global Variables}{6}{0}{3}
\beamer@subsectionintoc {3}{3}{auto}{7}{0}{3}
\beamer@subsectionintoc {3}{4}{goto}{8}{0}{3}
\beamer@sectionintoc {4}{Library Restrictions}{9}{0}{4}
\beamer@sectionintoc {5}{Formatting}{10}{0}{5}
\beamer@subsectionintoc {5}{1}{120 character-per-line limit}{10}{0}{5}
\beamer@subsectionintoc {5}{2}{Brace Placement for Non-Control Structures}{12}{0}{5}
\beamer@subsectionintoc {5}{3}{Brace Placement for Control Structures}{13}{0}{5}
\beamer@subsectionintoc {5}{4}{Indentation}{14}{0}{5}
\beamer@subsectionintoc {5}{5}{Identifiers}{15}{0}{5}
\beamer@sectionintoc {6}{Documentation Before Each Function}{16}{0}{6}
\beamer@sectionintoc {7}{General Documentation}{17}{0}{7}
\beamer@subsectionintoc {7}{1}{Comments at the Top of Files}{17}{0}{7}
\beamer@subsectionintoc {7}{2}{Naming Variables, Functions and Types}{18}{0}{7}
\beamer@subsectionintoc {7}{3}{Commenting Regions of Related Code}{21}{0}{7}
\beamer@sectionintoc {8}{Placement}{22}{0}{8}
\beamer@subsectionintoc {8}{1}{Statements and Conditions}{22}{0}{8}
\beamer@subsectionintoc {8}{2}{Prototype Grouping}{23}{0}{8}
\beamer@subsectionintoc {8}{3}{Grouping Related Code}{24}{0}{8}
\beamer@sectionintoc {9}{Code Structure}{25}{0}{9}
\beamer@subsectionintoc {9}{1}{Functional Decomposition}{25}{0}{9}
\beamer@subsectionintoc {9}{2}{Code Clarity}{26}{0}{9}
