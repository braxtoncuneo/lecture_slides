\beamer@sectionintoc {1}{A Crash Course on Servers and Terminals}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{A Brief History Lesson on Terminals}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{The File System}{5}{0}{1}
\beamer@subsectionintoc {1}{3}{The Teletype Machine (aka Teleprinter)}{15}{0}{1}
\beamer@subsectionintoc {1}{4}{The Command Line Interface}{17}{0}{1}
